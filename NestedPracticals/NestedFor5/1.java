/*
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
D4 C2 B2 A1
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Rows:");

		int rows=Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
			
			int num=rows;
			
			for(int j=1;j<=rows;j++){

				if(i%2==1){
					System.out.print((char)(64+num)+""+num+" ");
					num--;
				}else{
					System.out.print((char)(64+j)+""+j+" ");
				}
			}
			System.out.println();

		}

	}
}

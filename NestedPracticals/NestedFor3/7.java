/*
F 
E F 
D E F 
C D E F 
B C D E F 
A B C D E F 
*/

class Pattern{
	public static void main(String[] args){

		int N=6;

		for(int i=1;i<=N;i++){
			int Num=64+N-i+1;
			for(int j=1;j<=i;j++){

				System.out.print((char)Num++ + " ");
			}
			System.out.println();
		}
	}
}

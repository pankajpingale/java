/*
F 
E 1 
D 2 E 
C 3 D 4 
B 5 C 6 D 
A 7 B 8 C 9 
*/
class Pattern{
	public static void main(String[] args){

		int N=6;
		int Num1=1;

		for(int i=1;i<=N;i++){
			
		       	int Num2=64+N-i+1;
		
			for(int j=1;j<=i;j++){
		
				if(j%2==1){
		
					System.out.print((char)Num2++ + " ");
				}else{
					System.out.print(Num1++ + " ");
		
				}
			}
			System.out.println();
		}
	}
}

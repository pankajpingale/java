/*
1C3  4B2  9A1 
16C3 25B2 36A1 
49C3 64B2 81A1 
*/

class Pattern{
	public static void main(String[] args){

		int N=6;

		for(int i=1;i<=N;i++){
		
			int Num1=N;
			int Num2=64+N;
		
			for(int j=1;j<=N;j++){

				if(j%2==1){
	
					System.out.print((char)Num2 + " ");
					

				}else{
					System.out.print(Num1 + " ");
				

				}
				Num1--;
				Num2--;
			}
			System.out.println();
		}
	}
}

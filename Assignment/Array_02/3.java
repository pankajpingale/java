/*
sum of even elements
sum of odd elements
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		int sum1=0;
		int sum2=0;
		
		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<len;i++){

			if(arr[i]%2==0){

				sum1=sum1+arr[i];

			}else{
				
				sum2=sum2+arr[i];

			}
		}
		
		System.out.println("Sum of even elements:"+sum1);
		System.out.println("Sum of odd elements:"+sum2);
	
	}
}	

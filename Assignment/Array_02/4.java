/*
search elements
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];
	
		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		
		System.out.println("Enter elements you want:");

		int search=Integer.parseInt(br.readLine());

		int index=0;
		
		for(int i=0;i<len;i++){

			if(search==arr[i]){

				index++;
			}
		}
		
		System.out.println("Index of elemets is:"+index);
	
	}
}	

/*
Uncommon  elements
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr1[]=new int[len];
	
		System.out.println("Enter elements in first array:");
		
		for(int i=0;i<len;i++){

			arr1[i]=Integer.parseInt(br.readLine());
		}

		int arr2[]=new int[len];
		
		System.out.println("Enter elements in second array:");
		
		for(int i=0;i<len;i++){

			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Uncommon elements are:");
		
		int flag=0;

		for(int i=0;i<len;i++){

			for(int j=0;j<len;j++){

				if(arr1[i]==arr2[j]){
					
					flag++;
				}
			
			}

			if(flag==0){
			
				System.out.println(arr1[i]);
			}
			flag=0;
		}
		for(int i=0;i<len;i++){

			for(int j=0;j<len;j++){

				if(arr2[i]==arr1[j]){
					
					flag++;
				}
			
			}
			if(flag==0){
			
				System.out.println(arr2[i]);
			}
			flag=0;
		}
	}
}

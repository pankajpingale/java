/*
mimimum elements
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];
	
		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int temp=0;

		temp=arr[0];

		for(int i=0;i<len;i++){

			if(temp>arr[i]){

				temp=arr[i];
			}
		
		}
		
		System.out.println("min="+temp);
	
	}
}	

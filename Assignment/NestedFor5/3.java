/*
5 4 3 2 1
8 6 4 2 
9 6 3
8 4
5
*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows:");

		int rows=Integer.parseInt(br.readLine());
		
		int x=rows;
		for(int i=1;i<=rows;i++){
			
			int num=x*i;
			
			for(int j=1;j<=rows-i+1;j++){
			
				System.out.print(num+" ");
				num-=i;
			}
			x--;
			System.out.println();
		}

	}
}

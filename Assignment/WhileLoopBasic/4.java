//count odd digit of given No.

class Number{
	public static void main(String[] args){

		int num=942111423;
		int count=0;

		while(num!=0){
			if((num%10)%2==1){
				count++;
			}
			num/=10;
		}
		System.out.println(+count);
	}
}

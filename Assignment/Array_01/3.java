import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Length:");
		
		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter Array Element:");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int Product=1;
		int index=0;

		for(int i=0;i<arr.length;i++){

			if(index%2==1){

				Product=Product*arr[i];
			}
			index++;
		}

	/*	for(int i=1;i<arr.length;){
			
				
				Product=Product*arr[i];
				i+=2;
			}
	*/	
		System.out.println("Sum of even element is "+Product);

	}
}

			

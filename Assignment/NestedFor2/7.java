/*
1  2   9
4  25  6
49 8  81
*/

class Pattern{
	public static void main(String[] args){

		int N=3;
		int Num=1;

		for(int i=1;i<=N;i++){
			for(int j=1;j<=N;j++){
				
				if(j%2==1){

			 		System.out.print(Num*Num + " ");
					Num++;
				}else{

					System.out.print(Num++ + " ");
				
				}

			}
			System.out.println();
		}
	}
}

/*
Print Palindrome number index;
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int div=0;

		int index=-1;


		System.out.println("Palindrome Number index are:");

		for(int i=0;i<len;i++){

			index++;

			int sum=0;
			
			int temp=0;

			int temp1=arr[i];

			while(arr[i]!=0){

				temp=temp*10+(arr[i]%10);
				arr[i]=arr[i]/10;
			}
			
			if(temp==temp1){	
				
				System.out.println(+index);
			}
		}
	}
}	

/*
O 
14 13 
L K J 
9 8 7 6 
E D C B A 
*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows:");

		int rows=Integer.parseInt(br.readLine());
		
		int num=(rows*(rows+1))/2;
		
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=i;j++){
				
				if(rows%2==1){

					if(i%2==1){
	
						System.out.print((char)(64+num)+" ");
					}else{
						System.out.print(num+" ");
					}
					num--;
					
				}else{
					if(i%2==1){
	
						System.out.print(num+" ");
					}else{
						System.out.print((char)(64+num)+" ");
					}
					num--;
				}
			}
			System.out.println();


		}
	}
}

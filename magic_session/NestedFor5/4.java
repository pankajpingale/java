/*
i/p:start=2
    end=9
o/p=
   8 6 4 2
   3 5 7 9
*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter start No:");

		int start=Integer.parseInt(br.readLine());

		System.out.print("Enter end No:");

		int end=Integer.parseInt(br.readLine());

		for(int i=end;i>=start;i--){
			if(i%2==0){

				System.out.print(i+" ");	
			}
		}
		
		System.out.println();

		for(int j=start;j<=end;j++){
			if(j%2==1){
				
				System.out.print(j+" ");
			}
		}
	}
}

//difference between two character;
import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter first character :");

		char start=(br.readLine()).charAt(0);
	
		System.out.print("Enter second character :");

		char end=(br.readLine()).charAt(0);
	
		if(start==end){

			System.out.println("The two character are same");

		}else{

			int Dif=start-end;

			if(Dif<0){

				Dif=Dif*(-1);

			}

			System.out.println("The Difference between "+start+" & "+end+" is "+Dif);
		}

	}
}

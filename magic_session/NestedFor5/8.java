/*
$ 
@ @ 
& & & 
# # # # 
$ $ $ $ $ 
@ @ @ @ @ @ 
& & & & & & & 
# # # # # # # # 

*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows:");

		int rows=Integer.parseInt(br.readLine());
		
		int num=(rows*(rows+1))/2;
		
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=i;j++){
				
				if(i%4==1){

					System.out.print("$"+" ");

				}else if(i%4==2){
					
					System.out.print("@"+" ");
				
				}else if(i%4==3){
					
					System.out.print("&"+" ");
				
				}else if(i%4==0){
					
					System.out.print("#"+" ");
				}

			}System.out.println();
		}
	}
}

/*
Print Strong number index;
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int div=0;

		int index=-1;


		System.out.println("Strong Number index are:");

		for(int i=0;i<len;i++){

			index++;

			int temp=arr[i];

			int sum=0;
		
			while(arr[i]!=0){

				int mult=1;

				int rem=arr[i]%10;

				for(int j=1;j<=rem;j++){

					mult=mult*j;

				}
				sum=sum+mult;
				
				arr[i]=arr[i]/10;
			}
			if(sum==temp){
				
				System.out.println(+index);
			}
		}
	}
}	

/*
Print Perfect number index;
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int div=0;

		int index=-1;


		System.out.println("Perfect Number index are:");

		for(int i=0;i<len;i++){

			index++;

			int sum=0;
			
			for(int j=1;j<arr[i];j++){

				if(arr[i]%j==0){

					sum=sum+j;
				}

			}

			if(sum==arr[i]){	
				
				System.out.println(+index);
			}
		}
	}
}	

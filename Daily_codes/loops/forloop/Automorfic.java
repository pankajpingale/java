class Number{
	public static void main(String[] args){
		
		int num=32;
		int sqr=num*num;
		int realNo=num;
		int count=0;
		int temp=0;

		while(num!=0){
			count++;
		
			int rem1=num%10;
			int rem2=sqr%10;
		
			if(rem1==rem2){
				temp++;
			}
			num/=10;
			sqr/=10;
		}
		if(count==temp){
			System.out.println(realNo+" is Automorphic Number");
		}else{
			System.out.println(realNo+" is Not Automorphic Number");
		}
	}
}

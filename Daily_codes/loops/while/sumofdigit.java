/*
Given an Integer N
Print sum of its digit 
input:6531
o/p:15
*/

class Number{
	public static void main(String[] args){
		int N=6531;
		int sum=0;
		while(N!=0){
			sum=sum+N%10;
			N=N/10;
		}
		System.out.println(sum);
	}
}


/*
Integer N as input
print perfect squares till N
Perfect square : An integer whose square root is integer
input:30
o/p:1 4 9 16 25
*/
class Number{
	public static void main(String[] args){
		
		int N=30;
		int i=1;
		while(i*i<=N){
			System.out.println(i*i);
			i++;
		}
	}
}


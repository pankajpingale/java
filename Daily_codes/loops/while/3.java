/*
Integer N as input
Print odd integer from 1 to N using loop
*/

class Number{
        public static void main(String[] args){

                int N=10;
                int i=1;
                while(i<=N){
                        System.out.println(i);
                        i=i+2;
		}
	}
}  

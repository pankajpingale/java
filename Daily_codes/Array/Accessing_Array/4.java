import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Length:");
		
		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter Array Element:");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int sum=0;

		for(int i=0;i<arr.length;i++){
			
			sum=sum+arr[i];
		}
		
		System.out.println("Sum of array is "+sum);

	}
}

			

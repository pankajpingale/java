/*
No. of even elements
No. of odd elements
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		int count1=0;
		int count2=0;
		
		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<len;i++){

			if(arr[i]%2==0){

				count1++;
			}else{
				count2++;
			}
		}
		
		System.out.println("Number of even elements:"+count1);
		System.out.println("Number of odd elements:"+count2);
	
	}
}	

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Length:");
		
		int len=Integer.parseInt(br.readLine());

		char arr[]=new char[len];

		System.out.println("Enter Array Element:");

		for(int i=0;i<arr.length;i++){

			arr[i]=br.readLine().charAt(0);
		}

		System.out.println("Vowels Are:");
		
		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='E'||arr[i]=='O'||arr[i]=='U'){
			
				System.out.println(arr[i]);
			}
		}
	}
}

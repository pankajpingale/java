/*
1
2 c
4 e 6
7 h 9 i
*/

class Pattern{
	public static void main(String[] args){

		int N=4;
		int Num=1;
		char ch='A';

		for(int i=1;i<=N;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(Num++ + " ");
					ch++;
				}else{
					System.out.print(ch++ + " ");
					Num++;
				}
			}
			System.out.println();
		}
	}
}

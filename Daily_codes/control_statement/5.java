/*
Divisible by 4 or not
*/
class Number{
	public static void main(String[] args){
		int num=6;
		if(num%4==0)
			System.out.println(num+" is divisible by 4");
		else
			System.out.println(num+" is not divisible by 4");
	}
}


class StringBufferDemo{

	public static void main(String[] args){

		StringBuffer str=new StringBuffer("Pankaj");
		
		System.out.println(System.identityHashCode(str));

		str.append("Pingale");
		
		System.out.println(System.identityHashCode(str));

		System.out.println(str.capacity());
		
	}
}


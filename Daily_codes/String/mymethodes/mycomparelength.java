import java.io.*;
class Demo{
	
	static int mylength(String str){

		char[] arr=str.toCharArray();

		int count=0;

		for(int x:arr){

			count++;
		}
		return count;
	}
	static void mycomparelength(String str1,String str2){

		if(mylength(str1)==mylength(str2)){

			System.out.println("Length are equal");
		
		}else{

			System.out.println("Length are not equal");

		}
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter first String:");

		String str1=br.readLine();
		
		System.out.println("Enter second String:");
		
		String str2=br.readLine();

		mycomparelength(str1,str2);
	}
}


